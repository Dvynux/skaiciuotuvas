﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace skaičiuotuvas
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public void calc()
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double val1 = double.Parse(textBox1.Text);
            double val2 = double.Parse(textBox3.Text);
            double val3;
            if (textBox2.Text == "+")
            {
                val3 = val1 + val2;
                textBox4.Text = val3 + " ";
            }
            else if (textBox2.Text == "-")
            {
                val3 = val1 - val2;
                textBox4.Text = val3 + " ";
            }
            else if (textBox2.Text == "*")
            {
                val3 = val1 * val2;
                textBox4.Text = val3 + " ";
            }
            else if (textBox2.Text == "/")
            {
                val3 = val1 / val2;
                textBox4.Text = val3 + " ";
            }
        }
    }
}
